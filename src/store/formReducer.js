import * as types from './actionTypes';

const initialState = {
  openModal: false,
  successModal: false
};

const formreducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.SHOW_MODAL:
      return { ...state, openModal: state.openModal === false? true: false };
    case types.SHOW_SUCCESS:
      return { ...state, successModal: state.successModal === false? true: false };
    default:
      return state;
  }
}

export default formreducer;