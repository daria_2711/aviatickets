import * as types from './actionTypes';

const initialState = {
  tickets: [],
  ticketsByStops: [],
  stops: []
};

const ticketsreducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.TICKETS_FETCHED:
        const arr = action.tickets.map((ticket) => ({...ticket, price: ticket.price + ' rub'}));
    return { ...state, tickets: arr };
    
    case types.STOPS_CHANGED:
      if (action.stopsFilter === 'all'){
          state.stops.length = 0;
      }
      const { stops, tickets } = state;
      const indexStop = stops.indexOf(action.stopsFilter);
      if (indexStop === -1) {
        stops.push(action.stopsFilter);
      } else {
        stops.splice(indexStop, 1)
      }
      const newTickets = (tickets.filter(ticket => stops.some(stop => stop == ticket.stops)))
    return { ...state, stops, ticketsByStops: newTickets };

    case types.CURRENCY_RUB:
      const rubPrice = action.tickets.map((ticket) => ({...ticket, price: ticket.price + ' rub'}));
    return { ...state, tickets: rubPrice };

    case types.CURRENCY_USD:
      const usdPrice = action.tickets.map((ticket) => ({...ticket, price: Math.round(ticket.price/ 15) + ' usd'}));
    return { ...state, tickets: usdPrice};

    case types.CURRENCY_EUR:
      const eurPrice = action.tickets.map((ticket) => ({...ticket, price: Math.round(ticket.price/ 14) + ' eur'}));
    return { ...state, tickets: eurPrice};

    default:
      return state;
  }
}

export default ticketsreducer;