import * as types from './actionTypes';
import requestService from '../services/request';

// thunk dispatch action + async requst to server

export const fetchTickets = () => {
  return async (dispatch) => {
    try {
      const ticketsArray = await requestService.getDefaultTickets();
      ticketsArray.sort((a, b) => parseFloat(a.price) - parseFloat(b.price))
      dispatch({
        type: types.TICKETS_FETCHED,
        tickets: ticketsArray
      });
    } catch (error) {
      console.error(error);
    }
  };
}

export const updateStopsAC = (stopsCount) => ({type: types.STOPS_CHANGED, stopsFilter: stopsCount});

export const currencyRUB = () => {
  return async (dispatch) => {
    try {
      const ticketsRUB = await requestService.getDefaultTickets();
      ticketsRUB.sort((a, b) => parseFloat(a.price) - parseFloat(b.price));
      dispatch({
        type: types.CURRENCY_RUB,
        tickets: ticketsRUB
      });
    } catch (error) {
      console.error(error);
    }
  };
}

export const currencyUSD = () => {
  return async (dispatch) => {
    try {
      const ticketsUSD = await requestService.getDefaultTickets();
      ticketsUSD.sort((a, b) => parseFloat(a.price) - parseFloat(b.price));
      dispatch({
        type: types.CURRENCY_USD,
        tickets: ticketsUSD
      });
    } catch (error) {
      console.error(error);
    }
  };
}

export const currencyEUR = () => {
  return async (dispatch) => {
    try {
      const ticketsEUR = await requestService.getDefaultTickets();
      ticketsEUR.sort((a, b) => parseFloat(a.price) - parseFloat(b.price));
      dispatch({
        type: types.CURRENCY_EUR,
        tickets: ticketsEUR
      });
    } catch (error) {
      console.error(error);
    }
  };
}

export const showModalAC = (openModal) => {
  return { type: types.SHOW_MODAL, openModal };
}

export const showSuccessAC = () => {
  return { type: types.SHOW_SUCCESS };
}