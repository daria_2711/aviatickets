import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchTickets, updateStopsAC, showModalAC, showSuccessAC, currencyRUB, currencyUSD, currencyEUR } from '../store/actions';
import TicketsView from '../components/Tickets/TicketsView';
import SideBar from '../components/SideBar/SideBar';
import preload from '../images/tenor.gif';

import './Container.css'

class TicketsContainer extends Component {

  componentDidMount() {
    this.props.showTickets();
  }

  onStopsChanged = (stops) => {
    this.props.updateStops(stops);
  }

  setRUB = () => {
    this.props.currencyRUB()
  }

  setUSD = () => {
    this.props.currencyUSD()
  }

  setEUR = () => {
    this.props.currencyEUR()
  }

  onOpenModal = (openModal) => {
    this.props.showModal(openModal)
  }

  onCloseModal = (openModal) => {
    this.props.showModal(openModal);
  }

  onShowSuccess = () => {
    this.props.showSuccess();
  }

  render() {
    return (!this.props.tickets ? this.renderLoading() :
      <div className="ticketsScreen">
        <SideBar
          onStopsChanged={this.onStopsChanged}
          setRUB={this.setRUB}
          setUSD={this.setUSD}
          setEUR={this.setEUR} />
        <TicketsView
          tickets={this.props.tickets}
          ticketsByStops={this.props.ticketsByStops}
          successModal={this.props.successModal}
          openModal={this.props.openModal}
          onShowSuccess={this.onShowSuccess}
          onOpenModal={this.onOpenModal}
          onCloseModal={this.onCloseModal} />
      </div>
    );
  }

  renderLoading() {
    return (
      <div className="ticketsScreen">
        <img className="preload" src={preload} alt='preload' />
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    tickets: state.ticketsreducer.tickets,
    ticketsByStops: state.ticketsreducer.ticketsByStops,
    openModal: state.formreducer.openModal,
    successModal: state.formreducer.successModal
  };
}

let mapDipatchToProps = (dispatch) => {
  return {
    showTickets: () => {
      dispatch(fetchTickets())
    },
    updateStops: (stops) => {
      dispatch(updateStopsAC(stops))
    },
    currencyRUB: () => {
      dispatch(currencyRUB())
    },
    currencyUSD: () => {
      dispatch(currencyUSD())
    },
    currencyEUR: () => {
      dispatch(currencyEUR())
    },
    showModal: (openModal) => {
      dispatch(showModalAC(openModal))
    },
    showSuccess: () => {
      dispatch(showSuccessAC())
    }
  };
}
export default connect(mapStateToProps, mapDipatchToProps)(TicketsContainer);