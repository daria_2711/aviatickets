import React from 'react';
import CurrencyBtns from './CurrencyBtns';
import SelectStops from './SelectStops'
import './SideBar.css'

const SideBar = (props) => {

return(
    <div className='sideBar-container'>

        <h5>ВАЛЮТА</h5>
            <CurrencyBtns 
            setCurrentCurrency={props.setCurrentCurrency} 
            setRUB={props.setRUB}
            setUSD={props.setUSD}
            setEUR={props.setEUR}/>
        <h5>КОЛИЧЕСТВО ПЕРЕСАДОК</h5>
            <SelectStops 
            onStopsChanged = {props.onStopsChanged}
            />

    </div> 
    )
}

export default SideBar;