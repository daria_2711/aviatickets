import React from 'react';

import './SideBar.css'

const CurrencyBtns = (props) => {

return(
    <div className='change-cash'>
        <button className='currency-btn' onClick = { () =>{props.setRUB()}}>RUB</button>
        <button className='currency-btn' onClick = { () =>{props.setUSD()}}>USD</button>
        <button className='currency-btn' onClick = { () =>{props.setEUR()}}>EUR</button>
    </div>
    )
}

export default CurrencyBtns;