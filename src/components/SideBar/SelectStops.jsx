import React from 'react';

import './SideBar.css'

const SelectStops = (props) => {

return(
    <div className='filter-stops' onChange = {(e) => {props.onStopsChanged(e.target.value)}}>
        <label htmlFor='all'>
            <input type='checkbox'
            value='all'
            id='all'/> все </label>
        <label htmlFor='0'>
            <input 
            type='checkbox'
            id='0'
            value='0'/>без пересадок</label>
        <label htmlFor='1'>
            <input 
            type='checkbox'
            id='1'
            value='1'/> 1 пересадка</label>
        <label htmlFor='2'>
            <input 
            type='checkbox'
            id='2'
            value='2'/> 2 пересадки</label>
        <label htmlFor='3'>
            <input 
            type='checkbox' 
            id='3'
            value='3'/> 3 пересадки</label>
    </div>
    )
}

export default SelectStops;